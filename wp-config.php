<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'writerv4' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'By[/42m;%(4B9VzgFeT^tret Dl#Wk~umBZdk`6RU$dH|XS4aM^@O3o}uKj:f+fa' );
define( 'SECURE_AUTH_KEY',  'xp?;@h1>=_5 r]V,6{?q[{.HrT!iONLFhI/^qa^[Ma=M[=T~(cpadi(:m)|:K0`/' );
define( 'LOGGED_IN_KEY',    '&X9zM`{.kyn<Y_l1,-;)y8ED@K8_ayz,UDw98sLZz*bqHn}yCDgKvdu%Vd]tWf[i' );
define( 'NONCE_KEY',        ')(Z|QTP0v uppiocUC%}!}-`E?P$+vpxur*[&+gq JU$rXLEt]J Y(|`Q52r(rVn' );
define( 'AUTH_SALT',        'U]3-<[A96N@5=mNd# !K;f81s 6Wj& %pMby{@_Ys.0R+OGdGI1[HOEq4G_dTvgz' );
define( 'SECURE_AUTH_SALT', 'b|Tmn~)hg%>BC<OKM1Wx]nA.q$E(V%/gkB(R+USRv_f_HeD9;vI^Ya-EfPymKNX`' );
define( 'LOGGED_IN_SALT',   '$GeCUZ:gcJe;-lLs=VE8r6_j(cy@yCL9,^Y-npt/Y%9m&@{L$jQGG DIe;84e6qp' );
define( 'NONCE_SALT',       '(9.V LRpg4$I,3/NB{8P3F*&3;e {?E0kNMq*K`)y@CB+q_z7O6J;K8%7<uj7ebq' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
